<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Worksheet_model extends CI_Model{
	public $table   = 'worksheets';

	function __construct(){
		parent::__construct();
	}

	public function getWorksheetById($id)
	{
		return $this->db->from($this->table)->where('id',$id)->get()->row();
	}

	public function getAllWorksheet($id)
	{
		return $this->db->select('worksheets.*,users.name as username')
			->from($this->table)
			->join('users','users.id = worksheets.user_id')
			->where('worksheets.task_id',$id)->get()->result();
	}

	public function getTotalWorksheet()
	{
		return $this->db->select('worksheets.*,tasks.description,projects.name,users.name as username')
			->join('tasks','tasks.id = worksheets.task_id')
			->join('projects','projects.id = tasks.project_id')
			->join('users','users.id = worksheets.user_id')
			->from($this->table)
			->order_by('id','desc')->get()->result();
	}

	public function getTotalWorksheetById($id)
	{
		return $this->db->select('worksheets.*,tasks.description,projects.name,users.name as username')
			->join('tasks','tasks.id = worksheets.task_id')
			->join('projects','projects.id = tasks.project_id')
			->join('users','users.id = worksheets.user_id')
			->from($this->table)
			->where('tasks.user_id',$id)
			->order_by('id','desc')->get()->result();
	}
	
	public function getAllWorksheetByUserId($id){
		return $this->db->select('worksheets.*,users.name as username')
			->from($this->table)
			->join('users','users.id = worksheets.user_id')
			->where('task_id',$id)
			->get()->result();
	}

	public function insert($data)
	{
		return $this->db->insert($this->table,$data);
	}
}