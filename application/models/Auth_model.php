<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth_model extends CI_Model{
	public $table   = 'users';

	function __construct(){
		parent::__construct();
	}
	function login($data){
		return $this->db->where('email',$data['email'])->where('password',$data['password'])->from($this->table)->get()->row();
	}
}