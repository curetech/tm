<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Project_model extends CI_Model{
	public $table = 'projects';
	
	function __construct(){
		parent::__construct();
	}

	public function getById($id){
		return $this->db->from($this->table)->where('id',$id)->get()->row();
	}

	public function getAll()
	{
		return $this->db->from($this->table)->order_by('id','DESC')->get()->result();
	}

	public function getByUserId($id)
	{
		return $this->db->order_by('id','DESC')->get_where($this->table,['user_id',$id])->result();
	}
}