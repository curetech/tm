<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Task_model extends CI_Model{

	public $table   = 'tasks';

	function __construct(){
		parent::__construct();
	}

	public function getTaskById($id)
	{
		return $this->db->from($this->table)->where('id',$id)->get()->row();
	}

	public function getAllRunningTask($id)
	{
		return $this->db->select('tasks.*,COALESCE(SUM(worksheets.time),0) as total,users.name')
			->from($this->table)
			->join('worksheets','worksheets.task_id = tasks.id','LEFT')
			->join('users','users.id = tasks.user_id')
			->where('tasks.project_id',$id)
			->where('tasks.is_complete', 0)
			->group_by('tasks.id')
			->get()
			->result();
	}

	public function getAllCompleteTask($id)
	{
		return $this->db->select('tasks.*,COALESCE(SUM(worksheets.time),0) as total,users.name')
			->from($this->table)
			->join('worksheets','worksheets.task_id = tasks.id','LEFT')
			->join('users','users.id = tasks.user_id')
			->where('tasks.project_id',$id)
			->where('tasks.is_complete', 1)
			->group_by('tasks.id')
			->get()
			->result();
	}

	public function getAllRunningTaskByUserId($id){
		$userid = getAuthInfo()->id;
		return $this->db->select('tasks.*,COALESCE(SUM(worksheets.time),0) as total,users.name')
			->from($this->table)
			->join('worksheets','worksheets.task_id = tasks.id','LEFT')
			->join('users','users.id = tasks.user_id')
			->where('tasks.project_id',$id)
			->where('tasks.user_id',$userid)
			->where('tasks.is_complete', 0)
			->group_by('tasks.id')->get()->result();
	}

	public function getAllCompleteTaskByUserId($id){
		$userid = getAuthInfo()->id;
		return $this->db->select('tasks.*,COALESCE(SUM(worksheets.time),0) as total,users.name')
			->from($this->table)
			->join('worksheets','worksheets.task_id = tasks.id','LEFT')
			->join('users','users.id = tasks.user_id')
			->where('tasks.project_id',$id)
			->where('tasks.user_id',$userid)
			->where('tasks.is_complete', 1)
			->group_by('tasks.id')->get()->result();
	}

	public function getRunningTaskByUserId(){
		$userid = getAuthInfo()->id;
		return $this->db->select('tasks.*,COALESCE(SUM(worksheets.time),0) as total,users.name, projects.name as pname')
			->from($this->table)
			->join('worksheets','worksheets.task_id = tasks.id','LEFT')
			->join('users','users.id = tasks.user_id')
			->join('projects', 'projects.id = tasks.project_id')
			//->where('tasks.project_id',$id)
			->where('tasks.user_id',$userid)
			->where('tasks.is_complete', 0)
			->group_by('tasks.id')->get()->result();
	}

	public function getCompleteTaskByUserId(){
		$userid = getAuthInfo()->id;
		return $this->db->select('tasks.*,COALESCE(SUM(worksheets.time),0) as total,users.name, projects.name as pname')
			->from($this->table)
			->join('worksheets','worksheets.task_id = tasks.id','LEFT')
			->join('users','users.id = tasks.user_id')
			->join('projects', 'projects.id = tasks.project_id')
			//->where('tasks.project_id',$id)
			->where('tasks.user_id',$userid)
			->where('tasks.is_complete', 1)
			->group_by('tasks.id')->get()->result();
	}

	public function insert($data)
	{
		return $this->db->insert($this->table,$data);
	}
}
