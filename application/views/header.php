<!DOCTYPE html>
<html lang="en">
<head>
	<base href="<?= base_url() ?>">
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<title>Task Management - CureTech</title>

	<link rel="apple-touch-icon" sizes="180x180" href="assets/img/icons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/img/icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/img/icons/favicon-16x16.png">
	<link rel="manifest" href="assets/img/icons/site.webmanifest">
	<link rel="mask-icon" href="assets/img/icons/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
	<link href="assets/css/styles.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
</head>
<body class="sb-nav-fixed">
<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
	<!-- Navbar Brand-->
	<a class="navbar-brand ps-3" href="<?= base_url('/') ?>">TM-CureTech</a>
	<!-- Sidebar Toggle-->
	<!-- Navbar Search-->
	<div class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
		<!-- <a href="<?= base_url('/') ?>"><strong>CT Projects</strong></a>
		<a href="<?= base_url('worksheet') ?>"><strong>All Worksheet</strong></a>
		<a href="<?= base_url('tasklist') ?>"><strong>Your Tasklist</strong></a> -->
		<h5 style="color: #FFFFFF;">Hello <u><?php $user = getAuthInfo(); echo $user->name; ?>,</u> Welcome to TM</h5>
	</div>
	<!-- Navbar-->
	<ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
		<li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
			<ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
				<li><a class="dropdown-item disabled" href=""><?php $user = getAuthInfo(); echo $user->name; ?></a></li>
				<li><a class="dropdown-item" href="<?= base_url('/') ?>">CT Projects</a></li>
				<li><a class="dropdown-item" href="<?= base_url('worksheet') ?>">All Worksheet</a></li>
				<li><a class="dropdown-item" href="<?= base_url('tasklist') ?>">My Tasklist</a></li>
				<li><a class="dropdown-item" href="<?= base_url('logout') ?>">Logout</a></li>
			</ul>
		</li>
	</ul>
</nav>
<div class="container" style="margin-top: 80px">
	<div class="row">
		<?php if ($this->session->has_userdata('error')): ?>
		<div class="col-12">
			<div class="alert alert-danger" role="alert">
				<?=  $this->session->userdata('error')?>
			</div>
		</div>
		<?php endif;  ?>
		<?php if ($this->session->has_userdata('success')): ?>
			<div class="col-12">
				<div class="alert alert-success" role="alert">
					<?=  $this->session->userdata('success')?>
				</div>
			</div>
		<?php endif;  ?>
		<div class="col-12">



