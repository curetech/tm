<div>
	<div class="card mb-4">
		<div class="card-header d-flex justify-content-between">
			<div>
				<i class="fas fa-cogs me-1"></i> <a href="<?= base_url(); ?>">CT Projects</a> <i class="fas fa-table me-1"></i> Worksheet
			</div>
		</div>
		<div class="card-body">
			<table id="datatablesSimple">
				<thead>
				<tr>
					<th>SL</th>
					<th>Project</th>
					<th>Task + (Note)</th>
					<th>Time</th>
					<th>Date</th>
					<th>Owner</th>
					<!-- <th>Action</th> -->
				</tr>
				</thead>
				<tbody>
				<?php
					$user = getAuthInfo();
				?>
				<?php if (!empty($worksheets)): foreach ($worksheets as $key=>$work): ?>
					<tr>
						<td><?= $key+1 ?></td>
						<td><?= $work->name ?></td>
						<td><?= $work->description; if (empty($work->note)) { echo ""; } else { echo " <small>(<u><i>$work->note</i></u>)</small>"; } ?></td>
						<td><?= intdiv($work->time, 60) . ' h ' . ($work->time % 60) . ' m'; ?></td>
						<td><?php $date=date_create($work->date); echo date_format($date,"d-m-Y"); ?></td>
						<td><?=  $work->username?></td>
						<!-- <td>
							<a href="<?= base_url('work-delete/'.$work->id) ?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
						</td> -->
					</tr>
				<?php endforeach;endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
