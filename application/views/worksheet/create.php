<div class="row justify-content-center">
	<div class="col-12">
		<div class="card mb-4">
			<div class="card-header d-flex justify-content-between">
				<div>
					<i class="fas fa-cogs me-1"></i> <a href="<?= base_url(); ?>">CT Project</a> <i class="fas fa-arrow-right me-1"></i> <a href="<?= base_url('workspace/'.$project->id.'/task-list') ?>"><?= $project->name ?></a> <i class="fas fa-arrow-right me-1"></i> <a href="<?= base_url('workspace/'.$project->id.'/task/'.$task->id.'/worksheet') ?>"><?= $task->description ?></a> <i class="fas fa-table me-1"></i> Add Hours
				</div>
				<div>
					<a href="<?= base_url('workspace/'.$project->id.'/task/'.$task->id.'/worksheet') ?>" class="btn btn-success">Task Hours</a>
				</div>
			</div>
			<div class="card-body">
				<form action="<?= base_url('workspace/'.$project->id.'/task/'.$task->id.'/worksheet/create-action') ?>" method="POST">
					<input type="hidden" name="project_id" value="<?= $project->id ?>">
					<div class="form-group mt-2">
						<label for="">Time(Hours):</label>
						<input type="number" class="form-control" name="time">
						<?= form_error('time') ?>
					</div>
					<div class="form-group mt-2">
						<label for="">Time(Minute):</label>
						<input type="number" class="form-control" name="minute">
						<?= form_error('minute') ?>
					</div>
					<div class="form-group mt-2">
						<label for="note">Note:</label>
						<textarea name="note" id="note" class="form-control" placeholder="Optional"></textarea>
						<?= form_error('note') ?>
					</div>
					<button type="submit" class="btn btn-success mt-3">Submit</button>
				</form>
			</div>
		</div>
	</div>
</div>
