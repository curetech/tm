<div>
	<div class="card mb-4">
		<div class="card-header d-flex justify-content-between">
			<div>
				<i class="fas fa-cogs me-1"></i> <a href="<?= base_url(); ?>">CT Project</a> <i class="fas fa-arrow-right me-1"></i> <a href="<?= base_url('workspace/'.$project->id.'/task-list') ?>"><?= $project->name ?></a> <i class="fas fa-arrow-right me-1"></i> <?= $task->description ?>
			</div>
			<div>
				<?php  if ($task->is_complete != 1): ?>
				<a href="<?= base_url('workspace/'.$project->id.'/task/'.$task->id.'/worksheet/create') ?>" class="btn btn-success">Add Hours</a>
				<?php endif; ?>
			</div>

		</div>
		<div class="card-body">
			<table id="datatablesSimple">
				<thead>
				<tr>
					<th>SL</th>
					<th>Time</th>
					<th>Date</th>
					<th>Note</th>
					<th>Added By</th>
					<th>Action</th>
				</tr>
				</thead>
				<tbody>
				<?php
				$user = getAuthInfo();
				?>
				<?php if (!empty($worksheets)): foreach ($worksheets as $key=>$work): ?>
					<tr>
						<td><?= $key+1 ?></td>
						<td>
							<?php
							 echo intdiv($work->time, 60).' h '. ($work->time % 60) .' m';
							?>
						</td>
						<td><?php $date=date_create($work->date); echo date_format($date,"d-m-Y"); ?></td>
						<td><?= $work->note ?></td>
						<td><?= $work->username ?></td>
						<td>
							<a href="<?= base_url('work-delete/'.$work->id) ?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
				<?php endforeach;endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
