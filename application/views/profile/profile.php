<div>
	<div class="card mb-4">
		<div class="card-header d-flex justify-content-between">
			<div>
				<?php
				$user = getAuthInfo();
				echo "<h4><i class='fas fa-cog me-1'></i> $user->name <u>Running</u> Tasklist</h4>";
				?>
			</div>
		</div>
		<div class="card-body">
			<table id="datatablesSimple">
				<thead>
				<tr>
					<th>SL</th>
					<th>Project</th>
					<th>Task</th>
					<th>Start Date</th>
					<th>Delivery Date</th>
					<th>Total Hours</th>
					<th>Status</th>
					<!-- <th>Owner</th> -->
					<!-- <th>Action</th> -->
				</tr>
				</thead>
				<tbody>
					<?php if (!empty($runningtasks)): foreach ($runningtasks as $key=>$task): ?>
					<tr>
						<td><?= $key+1 ?></td>
						<td><?= $task->pname ?></td>
						<td><?= $task->description ?></td>
						<td>
							<?php
								$date=date_create($task->start_date);
								echo date_format($date,"d-m-Y");
							?>
						</td>
						<td>
							<?php
								if (!empty(@$task->close_date)){
									$d=date_create(@$task->close_date);
									echo date_format($d,"d-m-Y");
								}
							?>
						</td>
						<td><?= intdiv($task->total, 60) . ' h ' . ($task->total % 60) . ' m'; ?></td>
						<td>
							<?= $task->is_complete == 1 ? 'Complete':'Running' ?>
						</td>
						<!-- <td><?= $task->name ?></td> -->
						<!-- <td>
							<a href="<?= base_url('task-delete/'.$task->id) ?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
						</td> -->
					</tr>
					<?php endforeach;endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div>
	<div class="card mb-4">
		<div class="card-header d-flex justify-content-between">
			<div>
				<?php
				$user = getAuthInfo();
				echo "<h4><i class='fas fa-cog me-1'></i> $user->name <u>Complete</u> Tasklist</h4>";
				?>
			</div>
		</div>
		<div class="card-body">
			<table id="datatablesSimple2">
				<thead>
				<tr>
					<th>SL</th>
					<th>Project</th>
					<th>Task</th>
					<th>Start Date</th>
					<th>Delivery Date</th>
					<th>Total Hours</th>
					<th>Status</th>
					<!-- <th>Owner</th> -->
					<!-- <th>Action</th> -->
				</tr>
				</thead>
				<tbody>
					<?php if (!empty($completetasks)): foreach ($completetasks as $key=>$task): ?>
					<tr>
						<td><?= $key+1 ?></td>
						<td><?= $task->pname ?></td>
						<td><?= $task->description ?></td>
						<td>
							<?php
								$date=date_create($task->start_date);
								echo date_format($date,"d-m-Y");
							?>
						</td>
						<td>
							<?php
								if (!empty(@$task->close_date)){
									$d=date_create(@$task->close_date);
									echo date_format($d,"d-m-Y");
								}
							?>
						</td>
						<td><?= intdiv($task->total, 60) . ' h ' . ($task->total % 60) . ' m'; ?></td>
						<td>
							<?= $task->is_complete == 1 ? 'Complete':'Running' ?>
						</td>
						<!-- <td><?= $task->name ?></td> -->
						<!-- <td>
							<a href="<?= base_url('task-delete/'.$task->id) ?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
						</td> -->
					</tr>
					<?php endforeach;endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
