<div class="row justify-content-center">
	<div class="col-12">
		<div class="card mb-4">
			<div class="card-header d-flex justify-content-between">
				<div>
					<i class="fas fa-cog me-1"></i> Create Project
				</div>
				<div>
					<a href="<?= base_url('/') ?>" class="btn btn-success">Project List</a>
				</div>
			</div>
			<div class="card-body">
				<form action="<?= base_url('workspace/create-action') ?>" method="POST">
					<div class="form-group">
						<label for="">Project Name:</label>
						<input type="text" class="form-control" name="name">
						<?= form_error('name') ?>
					</div>
					<div class="form-group">
						<label for="">Manager Name:</label>
						<input type="text" class="form-control" name="manager">
						<?= form_error('manager') ?>
					</div>
					<button type="submit" class="btn btn-success mt-3">Submit</button>
				</form>
			</div>
		</div>
	</div>
</div>
