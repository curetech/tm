<div>
	<div class="card mb-4">
		<div class="card-header d-flex justify-content-between">
			<h4>
				<i class="fas fa-cogs me-1"></i>
				CT Projects
			</h4>
			<?php if (getAuthInfo()->role == 1): ?>
			<div>
				<a href="<?= base_url('workspace/create') ?>" class="btn btn-success">Create Project</a>
			</div>
			<?php endif; ?>
		</div>
		<div class="card-body">
			<table id="datatablesSimple">
				<thead>
				<tr>
					<th>SL</th>
					<th>Name</th>
					<th>Manager</th>
					<th>Total Task</th>
					<th>Total Hours</th>
					<?php //if (getAuthInfo()->role == 1): ?>
					<!-- <th>Action</th> -->
					<?php //endif; ?>
				</tr>
				</thead>
				<tbody>
				<?php if (!empty($projects)): foreach ($projects as $key => $project):?>
						<?php $totalHour = getTotalWork($project->id); ?>
					<tr>
						<td><?= $key+1 ?></td>
						<td><a href="<?= base_url('workspace/'.$project->id.'/task-list') ?>"><?=$project->name?></a></td>
						<td><?= $project->manager ?></td>
						<td><?= getTotalTask($project->id)->total ?></td>
						<td>
							<?php
							echo intdiv($totalHour, 60).' H '. ($totalHour % 60) .' M';
							?>
						</td>
						<?php //if (getAuthInfo()->role == 1): ?>
						<!-- <td>
							<a href="" class="btn btn-danger text-white"><i class="fa fa-trash"></i></a>
						</td> -->
						<?php //endif; ?>
					</tr>
				<?php endforeach; endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
