<div class="row justify-content-center">
	<div class="col-12">
		<div class="card mb-4">
			<div class="card-header d-flex justify-content-between">
				<div>
					<i class="fas fa-cogs me-1"></i> <a href="<?= base_url(); ?>">CT Project</a> <i class="fas fa-arrow-right me-1"></i> <a href="<?= base_url('workspace/'.$project->id.'/task-list') ?>"><?= $project->name ?> (<?= $project->manager ?>)</a> <i class="fas fa-table me-1"></i> Create Task
				</div>
				<div>
					<a href="<?= base_url('workspace/'.$project->id.'/task-list') ?>" class="btn btn-success"><?= $project->name ?> Task List</a>
				</div>
			</div>
			<div class="card-body">
				<form action="<?= base_url('workspace/'.$project->id.'/task/create-action') ?>" method="POST">
					<input type="hidden" name="project_id" value="<?= $project->id ?>">
					<div class="form-group">
						<label for="description">Description:</label>
						<textarea name="description" id="description" class="form-control"></textarea>
						<?= form_error('description') ?>
					</div>
					<div class="form-group mt-2">
						<label for="">Start Date:</label>
						<input type="date" class="form-control" name="start_date">
						<?= form_error('start_date') ?>
					</div>
					<div class="form-group mt-2">
						<label for="">Delivery Date:</label>
						<input type="date" class="form-control" name="close_date">
						<?= form_error('close_date') ?>
					</div>
					<button type="submit" class="btn btn-success mt-3">Submit</button>
				</form>
			</div>
		</div>
	</div>
</div>
