<div class="row justify-content-center">
	<div class="col-8">
		<div class="card mb-4">
			<div class="card-header d-flex justify-content-between">
				<div>
					<i class="fas fa-table me-1"></i>
					Task Create
				</div>
				<div>
					<a href="<?= base_url('workspace/'.$project->id.'/task-list') ?>" class="btn btn-success">Task List</a>
				</div>
			</div>
			<div class="card-body">
				<form action="<?= base_url('workspace/'.$project->id.'/task/update/'.$task->id) ?>" method="POST">
					<input type="hidden" name="project_id" value="<?= $project->id ?>">
					<div class="form-group">
						<label for="description">Description:</label>
						<textarea readonly name="description" id="description" class="form-control"><?= $task->description ?></textarea>
						<?= form_error('description') ?>
					</div>
					<div class="form-group mt-2">
						<label for="">Start Date:</label>
						<input readonly type="date" class="form-control" name="start_date" value="<?= $task->start_date ?>">
						<?= form_error('start_date') ?>
					</div>
					<div class="form-group mt-2">
						<label for="">Delivery Date:</label>
						<input type="date" class="form-control" name="close_date" value="<?= @$task->close_date ?>">
						<?= form_error('close_date') ?>
					</div>
					<div class="form-group mt-2">
						<div class="form-check">
							<input class="form-check-input" name="is_complete" type="checkbox" <?= $task->is_complete == 1 ? 'checked':'' ?> value="1" id="flexCheckDefault">
							<label class="form-check-label" for="flexCheckDefault">
								Mark as complete
							</label>
						</div>
					</div>
					<button type="submit" class="btn btn-success mt-3">Update</button>
				</form>
			</div>
		</div>
	</div>
</div>
