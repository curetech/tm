<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class HomeController extends MY_Controller {
	public function __construct() {
		parent::__construct();
		checkAuth();
		$this->load->model('Project_model');
		$this->load->model('Task_model');
		$this->load->model('Worksheet_model');
	}

	public function index() {
		$data['projects'] = $this->Project_model->getAll();
		$this->viewContent('home',$data);
	}

	public function userTaskList() {
		$userRole = getAuthInfo();
		$data['worksheets'] = $this->Worksheet_model->getTotalWorksheetById($userRole->id);
		//echo "<pre>"; print_r($data['worksheets']); exit();
		$data['runningtasks'] = $this->Task_model->getRunningTaskByUserId();
		//echo "<pre>"; print_r($data['runningtasks']); exit();
		$data['completetasks'] = $this->Task_model->getCompleteTaskByUserId();
		$this->viewContent('profile/profile',$data);
	}

	public function createWorkspace() {
		$this->viewContent('workspace/create');
	}

	public function createWorkspaceAction() {
		$this->_rules_workspace();

		if ($this->form_validation->run() == FALSE) {
			$this->createWorkspace();
		}else{
			$data['user_id'] = getAuthInfo()->id;
			$data['name'] = $this->input->post('name');
			$data['manager'] = $this->input->post('manager');
			$this->db->insert('projects',$data);
			$this->session->set_flashdata('success', 'Project Created Successfully');
			redirect(base_url('/'));
		}
	}
	function _rules_workspace(){
		$this->form_validation->set_error_delimiters('<span class="invalid-feedback d-block" role="alert">','</span>');
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('manager', 'manager', 'required');
	}

	public function taskList($id)
	{
		if (empty($id)){
			show_404();
		}
		$exist = $this->Project_model->getById($id);
		if (empty($exist)){
			redirect(base_url('/'));
		}
		$data['project'] = $exist;
		if (getAuthInfo()->role == 1){
			$data['runningtasks'] = $this->Task_model->getAllRunningTask($id);
			$data['completetasks'] = $this->Task_model->getAllCompleteTask($id);
		}else{

			$data['runningtasks'] = $this->Task_model->getAllRunningTaskByUserId($id);
			$data['completetasks'] = $this->Task_model->getAllCompleteTaskByUserId($id);
		}
		$this->viewContent('task/list',$data);
	}

	public function taskCreate($id)
	{
		$exist = $this->Project_model->getById($id);
		if (empty($exist)){
			redirect(base_url('/'));
		}
		$data['project'] = $exist;
		$this->viewContent('task/create',$data);
	}

	public function taskDelete($id)
	{
		$exist = $this->Task_model->getTaskById($id);
		if (empty($exist)){
			$this->session->set_flashdata('error','Task not found');
			redirect($this->agent->referrer());
		}
		$this->db->from('tasks')->where('id',$id)->delete();
		$this->session->set_flashdata('success','Task delete successful');
		redirect($this->agent->referrer());
	}

	public function taskEdit($projectId,$id)
	{
		if (empty($projectId) || empty($id)){
			$this->session->set_flashdata('error','Project Id or Task Id can not be empty');
			redirect($this->agent->referrer());
		}
		$task = $this->Project_model->getById($projectId);
		if (empty($task)){
			redirect(base_url('/'));
		}
		$exist = $this->Task_model->getTaskById($id);
		if (empty($exist)){
			$this->session->set_flashdata('error','Task not found');
			redirect($this->agent->referrer());
		}
		$data['project'] = $task;
		$data['task'] = $exist;
		$this->viewContent('task/edit',$data);
	}
	public function taskUpdate($projectId,$id){
		if (empty($projectId) || empty($id)){
			$this->session->set_flashdata('error','Project Id or Task Id can not be empty');
			redirect($this->agent->referrer());
		}
		$task = $this->Project_model->getById($projectId);
		if (empty($task)){
			redirect(base_url('/'));
		}
		$exist = $this->Task_model->getTaskById($id);
		if (empty($exist)){
			$this->session->set_flashdata('error','Task not found');
			redirect($this->agent->referrer());
		}
		$this->_rules_Task();
		if ($this->form_validation->run() == FALSE){
			$this->taskCreate($id);
		}else {
			$info['project_id'] = $projectId;
			$info['user_id'] = getAuthInfo()->id;
			$info['description'] = $this->input->post('description');
			$info['start_date'] = $this->input->post('start_date');
			$info['close_date'] = $this->input->post('close_date') ? :null;
			$info['is_complete'] = $this->input->post('is_complete') ? : 0;
			$this->db->where('id',$id)->update('tasks',$info);
			$this->session->set_flashdata('success', 'Task Update Successfully');
			redirect(base_url('workspace/'.$projectId.'/task-list'));
		}
	}
	public function taskCreateAction($id)
	{
		$exist = $this->Project_model->getById($id);
		if (empty($exist)){
			redirect(base_url('/'));
		}
		$this->_rules_Task();
		if ($this->form_validation->run() == FALSE){
			$this->taskCreate($id);
		}else {
			$info['project_id'] = $id;
			$info['user_id'] = getAuthInfo()->id;
			$info['description'] = $this->input->post('description');
			$info['start_date'] = $this->input->post('start_date');
			$info['close_date'] = $this->input->post('close_date') ? :null;
			$this->Task_model->insert($info);
			$this->session->set_flashdata('success', 'Task Created Successfully');
			redirect(base_url('workspace/'.$id.'/task-list'));
		}
	}
	function _rules_Task(){
		$this->form_validation->set_error_delimiters('<span class="invalid-feedback d-block" role="alert">','</span>');
		$this->form_validation->set_rules('description', 'description', 'required');
		$this->form_validation->set_rules('start_date', 'Start date', 'required');
	}

	public function worksheet($prId, $taskId)
	{
		if (empty($prId) || empty($taskId)){
			$this->session->set_flashdata('error','Project Id or Task Id can not be empty');
			redirect($this->agent->referrer());
		}
		$task = $this->Project_model->getById($prId);
		//echo "<pre>"; print_r($task); exit();
		if (empty($task)){
			redirect(base_url('/'));
		}
		$exist = $this->Task_model->getTaskById($taskId);
		if (empty($exist)){
			$this->session->set_flashdata('error','Task not found');
			redirect($this->agent->referrer());
		}
		$data['task'] = $exist;
		$data['project'] = $task;
		if (getAuthInfo()->role == 1){
			$data['worksheets'] = $this->Worksheet_model->getAllWorksheet($taskId);
		}else{
			$data['worksheets'] = $this->Worksheet_model->getAllWorksheetByUserId($taskId);
		}
		$this->viewContent('worksheet/list',$data);
	}

	public function worksheetCreate($prId,$taskId)
	{
		if (empty($prId) || empty($taskId)){
			$this->session->set_flashdata('error','Project Id or Task Id can not be empty');
			redirect($this->agent->referrer());
		}
		$task = $this->Project_model->getById($prId);
		if (empty($task)){
			redirect(base_url('/'));
		}
		$exist = $this->Task_model->getTaskById($taskId);
		if (empty($exist)){
			$this->session->set_flashdata('error','Task not found');
			redirect($this->agent->referrer());
		}
		$data['task']= $exist;
		$data['project']= $task;
		$this->viewContent('worksheet/create',$data);

	}

	public function worksheetCreateAction($prId,$taskId)
	{
		if (empty($prId) || empty($taskId)){
			$this->session->set_flashdata('error','Project Id or Task Id can not be empty');
			redirect($this->agent->referrer());
		}
		$task = $this->Project_model->getById($prId);
		if (empty($task)){
			redirect(base_url('/'));
		}
		$exist = $this->Task_model->getTaskById($taskId);
		if (empty($exist)){
			$this->session->set_flashdata('error','Task not found');
			redirect($this->agent->referrer());
		}
		$data['task']= $exist;
		$data['project']= $task;
		$this->_rules_Worksheet();
		if ($this->form_validation->run() == FALSE){
			$this->worksheetCreate($prId,$taskId);
		}else {
			$info['task_id'] = $taskId;
			$info['user_id'] = getAuthInfo()->id;
			$info['date'] = date('Y-m-d');
			$info['time'] = (intval($this->input->post('time')) * 60) + intval($this->input->post('minute'));
			$info['note'] = $this->input->post('note');
			$this->Worksheet_model->insert($info);
			$this->session->set_flashdata('success', 'Hours Added Successfully');
			redirect(base_url('workspace/'.$prId.'/task/'.$taskId.'/worksheet'));
		}
	}
	function _rules_Worksheet(){
		$this->form_validation->set_error_delimiters('<span class="invalid-feedback d-block" role="alert">','</span>');
		$this->form_validation->set_rules('minute', 'minute', 'required');
	}

	public function workDelete($id)
	{
		if (empty($id)){
			$this->session->set_flashdata('error','Id can not be empty');
			redirect($this->agent->referrer());
		}
		$exist = $this->Worksheet_model->getWorksheetById($id);
		if (empty($exist)){
			$this->session->set_flashdata('error','Worksheet not found');
			redirect($this->agent->referrer());
		}
		$this->db->where('id',$id)->delete('worksheets');
		$this->session->set_flashdata('success','Worksheet delete Successful');
		redirect($this->agent->referrer());
	}

	public function worksheetAll()
	{
		$userRole = getAuthInfo();

		if ($userRole->role == 1){
			$data['worksheets'] = $this->Worksheet_model->getTotalWorksheet();
			//echo "<pre>"; print_r($data['worksheets']); exit();
		}else{
			$data['worksheets'] = $this->Worksheet_model->getTotalWorksheetById($userRole->id);
		}
		$this->viewContent('worksheet/all',$data);
	}
}