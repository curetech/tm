<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AuthController extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Auth_model');

	}

	public function login()
	{
		$exist = $this->session->userdata('login');
		if (!empty($exist)){
			redirect(base_url('/'));
		}
		$this->load->view('login');
	}

	public function login_action()
	{
		$this->_rules();
		if ($this->form_validation->run() == FALSE){
			$this->login();
		}else{
			$data['email'] = $this->input->post('email');
			$data['password'] = $this->input->post('password');
			$userdata = $this->Auth_model->login($data);
			if (empty($userdata)){
				$this->session->set_flashdata('error','Email/Password incorrect');
				redirect(base_url('login'));
			}else{
				$this->session->set_userdata('login',$userdata->id);
				redirect(base_url('/'));
			}
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('login');
		redirect(base_url('/login'));
	}
	function _rules(){
		$this->form_validation->set_error_delimiters('<span class="invalid-feedback d-block" role="alert">','</span>');
		$this->form_validation->set_rules('email', 'email', 'trim|required');
		$this->form_validation->set_rules('password', 'password', 'trim|required');
	}
	
}
