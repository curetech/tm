<?php
defined('BASEPATH') OR exit('No direct script access allowed');
function pp($data) {
	echo '<pre style="padding:10px;">';
	print_r($data);
	echo '</pre>';
	exit();
}
function checkAuth(){
	$ci =& get_instance();
	$session = $ci->session->userdata('login');
	if (empty($session)){
		return redirect('login');
	}else{
		return  true;
	}
}
function getAuthInfo(){
	$ci =& get_instance();
	$session = $ci->session->userdata('login');
	if (empty($session)){
		return false;
	}else{
		return $ci->db->get_where('users',['id'=>$session])->row();
	}
}

function getTotalTask($projectId = 0){
	$ci =& get_instance();
	return $ci->db->select('count(id) as total')->from('tasks')->where('project_id',$projectId)->get()->row();
}
function getTotalWork($projectId = 0){
	$ci =& get_instance();
	$data =  $ci->db->select('COALESCE(SUM(worksheets.time),0) as total_hour')
		->from('tasks')
		->join('worksheets','worksheets.task_id = tasks.id')
		->where('tasks.project_id',$projectId)
		->group_by('tasks.id')->get()->result();
	return array_sum(array_column($data,'total_hour'));

}
